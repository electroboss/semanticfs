#!/usr/bin/env python
import logging, stat, time, os, sys, csv, errno, pickle

from copy import deepcopy as dc
from fuse import FUSE, FuseOSError, Operations, LoggingMixIn, fuse_get_context


class SemanticFS(LoggingMixIn, Operations):
  def __init__(self, datapath=".data"):
    self.datapath = datapath
    self.reload()
  
  def reload(self):
    self.filelist = {}
    self.tags = []
    self.changes = []

    with open(os.path.join(self.datapath,"tags.csv"), "r") as tagsf:
      reader = csv.reader(tagsf)
      for row in reader:
        filepath = row[0]
        filename = filepath.split(os.path.sep)[-1]
        assert os.path.exists(filepath), FileNotFoundError("No such file or directory "+filepath)
        if filename in self.filelist.keys():
          raise ValueError(filename + " already in filedatabase as " + self.filelist[filename]["path"])
        self.filelist[filename] = {"path": filepath, "tags": row[1:]}
        for tag in row[1:]:
          if tag in self.tags:
            continue
          else:
            self.tags.append(tag)
      tagsf.close()

    delta_files = sorted(os.listdir(os.path.join(self.datapath,"deltas")))
    for delta_file in delta_files:
      with open(os.path.join(self.datapath,"deltas",delta_file), "rb") as deltaf:
        delta = pickle.load(deltaf)
        deltaf.close()
        for added_tags in delta[0].keys():
          for tag in delta[0][added_tags]:
            self.filelist[added_tags]["tags"].append(tag)
          if tag not in self.tags:
            self.tags.append(tag)
        for removed_tags in delta[1].keys():
          for tag in delta[1][removed_tags]:
            self.filelist[removed_tags]["tags"].remove(tag)

  def get_tags_from_path(self, path):
    while path[-2:] == "/.":
      path = path[:-2]
    return path.split("/")[2:]
  
  def get_files_with_tags(self, tags):
    logging.debug("Get files with tags:",tags)
    return [f for f in self.filelist if all([tag in self.filelist[f]["tags"] for tag in tags])]

  def parse_path(self, path):
    strpath = dc(path)
    if strpath == "/":
      return ["root"]
    path = path.split("/")[1:] # get rid of "" e.g. "/test" → ["","test"] → ["test"]
    if path[0] == "tags":
      if len(path) == 1:
        return ["tagdir", []]
      if path[-1] == "files":
        #return ["querydir", [x[1:] if x in self.tags else " ".join(x.split(" ")[1:]) if " ".join(x.split(" ")[1:]) in self.tags else " ".join(x.split(" ")[:-1]) for x in path[1:-1]]] # Split gets rid of number, but if x is a tag itself it doesn't need trimming
        #return ["querydir", [x if x in self.tags else " ".join(x.split(" ")[:-1]) for x in path[1:-1]]] # Split gets rid of number, but if x is a tag itself it doesn't need trimming
        return ["querydir", path[1:-1]]
      if path[-2] == "files":
        return ["queryfile", path[-1]]
      #return ["tagdir", [x[1:] if x in self.tags else " ".join(x.split(" ")[1:]) if " ".join(x.split(" ")[1:]) in self.tags else " ".join(x.split(" ")[:-1]) for x in path[1:]]] # Split gets rid of number, but if x is a tag itself it doesn't need trimming
      #return ["tagdir", [x if x in self.tags else " ".join(x.split(" ")[:-1]) for x in path[1:]]] # Split gets rid of number, but if x is a tag itself it doesn't need trimming
      return ["tagdir", path[1:]]
    if path[0] == "files":
      if len(path) == 1:
        return ["files"]
      if len(path) == 2:
        if path[1] in self.filelist.keys():
          return ["edittags", path[1]]
      if len(path) == 3:
        if path[1] in self.filelist.keys():
          return ["edittagstag", path[1], path[2]]
    raise FuseOSError(errno.ENOENT)

  def create(self, path, mode):
    parsed_path = self.parse_path(path)
    if parsed_path[0] != "edittagstag":
      raise FuseOSError(errno.EACCES)
    if parsed_path[2] in self.filelist[parsed_path[1]]["tags"]:
      raise FuseOSError(errno.EEXIST)
    self.filelist[parsed_path[1]]["tags"].append(parsed_path[2])
    self.changes.append(
      ["add_tag",parsed_path[1],parsed_path[2]]
    )
    if parsed_path[2] not in self.tags:
      self.tags.append(parsed_path[2])
    return 0
  
  def unlink(self, path):
    parsed_path = self.parse_path(path)
    if parsed_path[0] != "edittagstag":
      raise FuseOSError(errno.EACCES)
    if parsed_path[2] not in self.filelist[parsed_path[1]]["tags"]:
      raise FuseOSError(errno.ENOENT)
    self.filelist[parsed_path[1]]["tags"].remove(parsed_path[2])
    self.changes.append(
      ["remove_tag",parsed_path[1],parsed_path[2]]
    )
    # TODO: remove isolated tags (ones with no files)

  def destroy(self, path):
    # Separate pickle file so as to not corrupt csv.
    delta = [
      {}, # files and their added tags
      {}, # files and their removed tags
    ]
    for change in self.changes:
      if change[0] == "add_tag":
        if change[1] not in delta[0]:
          delta[0][change[1]] = []
        delta[0][change[1]].append(change[2])
      if change[0] == "remove_tag":
        if change[1] not in delta[1]:
          delta[1][change[1]] = []
        delta[1][change[1]].append(change[2])
    if delta == [{},{}]:
      return # Don't waste filespace with empty files
    # get the earliest free filename
    delta_filenames = os.listdir(os.path.join(self.datapath,"deltas"))
    fileno=0
    while "delta"+str(fileno)+".dat" in delta_filenames:
      fileno += 1
    filename = "delta"+str(fileno)+".dat"
    with open(os.path.join(self.datapath,"deltas",filename), "wb") as deltaf:
      pickle.dump(delta, deltaf)
      deltaf.close()

  def readdir(self, path, fh):
    parsed_path = self.parse_path(path)
    if parsed_path[0] == "tagdir":
      selected_tags = parsed_path[1]
      unselected_tags = [tag for tag in dc(self.tags) if (not (tag in selected_tags)) and len(self.get_files_with_tags(selected_tags + [tag]))]
      
      return ([
        ".",
        "..",
        "files",
      ]
       #+ [str(len(self.get_files_with_tags(selected_tags+[tag]))) + " " + tag for tag in unselected_tags] # "[number] [tag]" for each tag
       #+ [tag + " " + str(len(self.get_files_with_tags(selected_tags+[tag]))) for tag in unselected_tags] # Other way round for alphabetic navigation, exclamation so they come first
       + unselected_tags # + tags on their own for good measure
      )
    if parsed_path[0] == "querydir":
      return [
        ".",
        ".."
      ] + self.get_files_with_tags(parsed_path[1])
    if parsed_path[0] == "root":
      return [".", "..", "tags", "files"]
    if parsed_path[0] == "files":
      return [".", ".."] + list(self.filelist.keys())
    if parsed_path[0] == "edittags":
      if parsed_path[1] in self.filelist.keys():
        return [".", ".."] + self.filelist[parsed_path[-1]]["tags"]
    raise FuseOSError(errno.ENOENT)
  
  def readlink(self, path, fh=None):
    parsed_path = self.parse_path(path)
    if not (parsed_path[1] in self.filelist.keys()):
      raise FuseOSError(errno.ENOENT)
    if not parsed_path[0] == "queryfile":
      raise FuseOSError(errno.EINVAL)

    return self.filelist[parsed_path[1]]["path"]

  def getattr(self, path, fh=None):
    parsed_path = self.parse_path(path)

    logging.debug(parsed_path)

    st = {}
    now = time.time()
    st['st_ino']   = 0
    st['st_dev']   = 0
    st['st_nlink'] = 1
    st['st_uid']   = os.getuid() #file object's user id
    st['st_gid']   = os.getgid() #file object's group id
    st['st_size']  = 100    #size in bytes
    st['st_atime'] = now  #last access time in seconds
    st['st_mtime'] = now  #last modified time in seconds
    st['st_ctime'] = now
    st['st_blocks'] = (int) ((st['st_size'] + 511) / 512)
    
    if parsed_path[0] in ["tagdir", "querydir", "root", "files", "edittags"]:
      st["st_mode"] = 0o444 | stat.S_IFDIR
    elif parsed_path[0] == "queryfile":
      st["st_mode"] = 0o777 | stat.S_IFLNK
    elif parsed_path[0] == "edittagstag":
      if parsed_path[2] not in self.filelist[parsed_path[1]]["tags"]:
        raise FuseOSError(errno.ENOENT) # If the tag isn't a tag, it doesn't exist (otherwise will cause overwrite popup for everything)
      st["st_mode"] = 0o222 | stat.S_IFREG
    else:
      raise ValueError("I messed something up", path, parsed_path)
    
    logging.debug(oct(st["st_mode"]))

    return st


if __name__ == '__main__':
  if not sys.argv[1]:
    raise Exception("No path to mount to specified. Usage \"python3 . path/to/mountpoint\"")
  logging.basicConfig(level=logging.DEBUG)
  fuse = FUSE(
    SemanticFS("data"), sys.argv[1], foreground=True, allow_other=True)
